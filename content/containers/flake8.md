---
title: flake8
anchor: flake8
link: 'https://gitlab.com/pipeline-components/flake8'
dockerimage: 'python:3.7.5-alpine3.10'
---
The image is for running flake8, The image is based on python:3.7-alpine3.8

<!--more-->
## Examples

```yaml
flake8:
  stage: linting
  image: pipelinecomponents/flake8:latest
  script:
    - flake8 --verbose .
```
