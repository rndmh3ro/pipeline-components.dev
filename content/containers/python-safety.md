---
title: python-safety
anchor: python-safety
link: 'https://gitlab.com/pipeline-components/python-safety'
dockerimage: 'python:3.7.5-alpine3.10'
---
The image is for running safety. The image is based on python:3.7-alpine3.8.
The variable `SAFETY_API_KEY` can be used to set if you have a paid api key.

For more information: [pyup safety][pyuplink]

<!--more-->
## Examples

```yaml
python-safety:
  stage: linting
  image: pipelinecomponents/python-safety:latest
  script:
    - safety check --full-report -r requirements.txt
```
