---
title: "Gitlab"
anchor: "gitlab"
---

A partial example from a production environment
<!--more-->
```yml
---
stages:
  - preflight

# Preflight
.preflight: &preflight
  stage: preflight

phpcs PSR2:
  <<: *preflight
  image: pipelinecomponents/php-codesniffer:latest
  script:
    - phpcs -s -p --colors --extensions=php --standard=PSR2 .
  except:
    variables:
      - $DISABLE_PHP_PSR2

phpcs PSR12:
  <<: *preflight
  image: pipelinecomponents/php-codesniffer:latest
  script:
    - phpcs -s -p --colors --extensions=php --standard=PSR12 .
  except:
    variables:
      - $DISABLE_PHP_PSR12

phplint:
  <<: *preflight
  image: pipelinecomponents/php-linter:latest
  script:
    - parallel-lint --colors .
  except:
    variables:
      - $DISABLE_PHPLINT

php security-checker:
  <<: *preflight
  image: pipelinecomponents/php-security-checker:latest
  script:
    cd ${COMPOSER_LOCATION:-.} && security-checker security:check composer.lock
  except:
    variables:
      - $DISABLE_PHP_SECURITYCHECKER

jsonlint:
  <<: *preflight
  image: pipelinecomponents/jsonlint:latest
  before_script:
    - jsonlint --version || true
  script:
    - |
      find . -not -path './.git/*' -name '*.json' -type f -print0 |
      parallel --will-cite -k -0 -n1 jsonlint -q
  except:
    variables:
      - $DISABLE_JSONLINT

yamllint:
  <<: *preflight
  image: pipelinecomponents/yamllint:latest
  before_script:
    - yamllint --version
  script:
    - yamllint .
  except:
    variables:
      - $DISABLE_YAMLLINT

eslint:
  <<: *preflight
  image: pipelinecomponents/eslint:latest
  before_script:
    - touch dummy.js
    - eslint --version
  script:
    - eslint $( [[ -e .eslintrc ]] || echo '--no-eslintrc' ) --color .
  except:
    variables:
      - $DISABLE_ESLINT

stylelint:
  <<: *preflight
  image: pipelinecomponents/stylelint:latest
  script:
    - stylelint --color '**/*.css'
  except:
    variables:
      - $DISABLE_STYLELINT
```
