---
title: markdown-spellcheck
anchor: markdown-spellcheck
link: 'https://gitlab.com/pipeline-components/markdown-spellcheck'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running markdown-spellcheck, markdown-spellcheck is installed in /app/ in case you need to customize the install before usage.
The image is based on alpine:3.8.
Upstream [Markdown Spellcheck][markdown-spellcheck]

<!--more-->
## Examples

```yaml
markdown-spellcheck:
  stage: linting
  image: pipelinecomponents/markdown-spellcheck:latest
  script:
    - mdspell --report '**/*.md'
```
