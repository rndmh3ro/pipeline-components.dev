---
title: markdownlint
anchor: markdownlint
link: 'https://gitlab.com/pipeline-components/markdownlint'
dockerimage: 'ruby:2.6.5-alpine3.10'
---
The image is for running markdownlint, markdownlint is installed in /app/ in case you need to customize the install before usage.
The image is based on ruby:2.6.5-alpine3.10

More information about rules and configuration could be found here: [Docs][markdownlint]

<!--more-->
## Examples

```yaml
markdownlint:
  stage: linting
  image: pipelinecomponents/markdownlint:latest
  script:
    - mdl --style all --warnings .
```
