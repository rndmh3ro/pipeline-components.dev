---
title: stylelint
anchor: stylelint
link: 'https://gitlab.com/pipeline-components/stylelint'
dockerimage: 'node:12.13.0-alpine'
---
The application is installed in /app/ and base the following components

-   stylelint
-   stylelint-config-standard

> Style lint is a bit problematic and a wrapper script tries to work around this.
> If you want to use your own stylelint config packages, set CONFIG_BASEDIR to the directory where node_modules is located

<!--more-->
## Examples

```yaml
stylelint:
  stage: linting
  image: pipelinecomponents/stylelint:latest
  script:
    - stylelint --color '**/*.css'
```
