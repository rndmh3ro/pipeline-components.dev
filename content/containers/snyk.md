---
title: snyk
anchor: snyk
link: 'https://gitlab.com/pipeline-components/snyk'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running [snyk], snyk is installed in /app/ in case you need to customize the install before usage.
The image is based on node:10.14-alpine
This requires a local available docker service like `dind` or a shared docker socket, docker is installed in this container.

<!--more-->
## Examples

```yaml
snyk:
  stage: linting
  variables:
    SNYK_TOKEN: my-secret-snyk-token
  image: pipelinecomponents/snyk:latest
  script:
    - snyk test --docker pipelinecomponents/php-linter:latest --file=Dockerfile
```

For this example:

-   pipelinecomponents/php-linter:latest is the container to scan, and should be localy available
-   SNYK_TOKEN should be set as a secret variable in gitlab
