---
title: ansible-lint
anchor: ansible-lint
link: 'https://gitlab.com/pipeline-components/ansible-lint'
dockerimage: 'alpine:3.10.3'
---
The image is for running Ansible-lint, The image is based on alpine:3.8

<!--more-->
## Examples

```yaml
ansible-lint:
  stage: linting
  image: pipelinecomponents/ansible-lint:latest
  script:
    - ansible-lint .
```
