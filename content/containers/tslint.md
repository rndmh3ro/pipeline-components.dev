---
title: tslint
anchor: tslint
link: 'https://gitlab.com/pipeline-components/tslint'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running tslint, tslint is installed in /app/ in case you need to customize the install before usage.
The image is based on node:10.15-alpine

for more information on using tslint check their [website][tslintwebsite]

<!--more-->
## Examples

```yaml
tslint:
  stage: linting
  image: pipelinecomponents/tslint:latest
  script:
    - tslint 'src/**/*.ts'
```
