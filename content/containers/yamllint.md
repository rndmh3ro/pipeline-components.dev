---
title: yamllint
anchor: yamllint
link: 'https://gitlab.com/pipeline-components/yamllint'
dockerimage: 'python:3.7.5-alpine3.10'
---
The image is for running yamllint, yamllint is installed in /app/ in case you need to customize the install before usage

<!--more-->
## Examples

```yaml
yamllint:
  stage: linting
  image: pipelinecomponents/yamllint:latest
  script:
    - yamllint .
```
