
const gitlab = require('gitlab');
const remark = require('remark');
const filter = require('unist-util-filter');
const map = require('unist-util-map');
const fs = require('fs').promises;
const yaml = require('js-yaml');


const api = new gitlab.Gitlab({
  host: 'http://gitlab.com',
});

const projects = api.GroupProjects.all(3749748);

projects.then(projectWalker);

function projectWalker(projects) {
  projects.map(handlerProject);
}

async function handlerProject(project) {
  let content = getDocumentation(project);
  let meta = getMetaData(project);

  content = await content;
  meta = await meta;

  const data = yaml.dump({
    title: project.name,
    anchor: project.name,
    link: project.web_url,
    dockerimage: meta.base,
  });

  const pattern = /^#/m
  content = content.replace(pattern, "<!--more-->\n#");

  await fs.writeFile(`${project.name}.md`,
    `---\n${
    data
    }---\n${
    content}`);
}


async function getDocumentation(project) {
  let keep = false;
  const contents = await api.RepositoryFiles.showRaw(project.path_with_namespace, 'README.md', 'master');

  const tree = remark().parse(contents);

  let newTree = filter(tree, (node) => {
    if (node.type == 'root') {
      return true;
    }

    if (node.type == 'definition') {
      return true;
    }

    if (node.type == 'heading' && node.depth == 2) {
      if (
        node.children[0].value == 'Usage'
      ) {
        keep = true;
        return false;
      }
      if (
        node.children[0].value == 'Examples'
        || node.children[0].value == 'Example'
      ) {
        keep = true;
        return true;
      }
      keep = false;
    }
    return keep;
  });

  newTree = map(newTree, (node) => {
    if (node.type == 'heading' && node.depth == 2) {
      // node.depth = 4;
    }
    return node;
  });

  return remark().stringify(newTree);
}

async function getMetaData(project) {
  const line = api.RepositoryFiles.showRaw(project.path_with_namespace, 'Dockerfile', 'master')
    .then((content) => content.split('\n').filter((line) => line.match(/^FROM/)).pop().split(' ')
      .pop()
      .split('@')
      .shift());
  const meta = {
    base: await line,
  };
  return meta;
}
