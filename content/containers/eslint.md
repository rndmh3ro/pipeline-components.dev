---
title: eslint
anchor: eslint
link: 'https://gitlab.com/pipeline-components/eslint'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running eslint, eslint is installed in /app/ in case you need to customize the install before usage

<!--more-->
## Examples

```yaml
eslint:
  stage: linting
  image: pipelinecomponents/eslint:latest
  before_script:
    - touch dummy.js
  script:
    - eslint $( [[ -e .eslintrc ]] || echo '--no-eslintrc' ) --color .

```

Touching dummy.js prevents eslint from complaining that it had no files to lint
