---
title: phpunit
anchor: phpunit
link: 'https://gitlab.com/pipeline-components/phpunit'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image is for running phpunit, phpunit is installed in /app/ in case you need to customize the install before usage.
The image is based on php:5.6-alpine3.8

<!--more-->
## Examples

```yaml
phpunit:
  stage: linting
  image: pipelinecomponents/phpunit:latest
  script:
    - phpunit .
```
