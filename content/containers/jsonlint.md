---
title: jsonlint
anchor: jsonlint
link: 'https://gitlab.com/pipeline-components/jsonlint'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running jsonlint, jsonlint is installed in /app/ in case you need to customize the install before usage.
The image is based on node:10.14-alpine

<!--more-->
## Examples

```yaml
jsonlint:
  stage: linting
  image: pipelinecomponents/jsonlint:latest
  script:
    - |
      find . -not -path './.git/*' -name '*.json' -type f -print0 |
      parallel --will-cite -k -0 -n1 jsonlint -q
```
