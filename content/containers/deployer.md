---
title: deployer
anchor: deployer
link: 'https://gitlab.com/pipeline-components/deployer'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image has for running deployer in a small container and supporting all of
the default deployer recipes

<!--more-->
## Examples

```yaml
# Deploy templates
.deploy: &deploy
  stage: deploy
  image: pipelinecomponents/deployer:latest
  dependencies: []
  before_script:
    - eval $(ssh-agent -s)
    - >-
      [[ ${CI_ENVIRONMENT_NAME:-local} == "testing"  && ! -z ${TESTING_DEPLOYMENT_KEY} ]] &&
      echo "${TESTING_DEPLOYMENT_KEY}"    | tr -d "\r" | ssh-add -
    - >-
      [[ ${CI_ENVIRONMENT_NAME:-local} == "acceptance" && ! -z ${ACCEPTANCE_DEPLOYMENT_KEY} ]] &&
      echo "${ACCEPTANCE_DEPLOYMENT_KEY}" | tr -d "\r" | ssh-add -
    - >-
      [[ ${CI_ENVIRONMENT_NAME:-local} == "production" && ! -z  ${PRODUCTION_DEPLOYMENT_KEY} ]] &&
      echo "${PRODUCTION_DEPLOYMENT_KEY}" | tr -d "\r" | ssh-add -
  script:
    - dep deploy ${CI_ENVIRONMENT_NAME:-local}

deploy production:
  <<: *deploy
  only:
    - /^v[0-9.]+$/
  environment:
    name: production
```

-   `*_DEPLOYMENT_KEY` contains the ssh key used for accessing the deployment server
-   `CI_ENVIRONMENT_NAME` environment defined by gitlab
