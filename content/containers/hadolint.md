---
title: hadolint
anchor: hadolint
link: 'https://gitlab.com/pipeline-components/hadolint'
dockerimage: 'alpine:3.10.3'
---
The image is for running hadolint, The image is based on alpine:3.8

<!--more-->
## Examples

```yaml
hadolint:
  stage: linting
  image: pipelinecomponents/hadolint:latest
  script:
    - hadolint Dockerfile
```
