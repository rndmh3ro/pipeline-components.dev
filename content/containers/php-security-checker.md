---
title: php-security-checker
anchor: php-security-checker
link: 'https://gitlab.com/pipeline-components/php-security-checker'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image is for running security-checker, security-checker is installed in /app/ in case you need to customize the install before usage. The image is based on php:7.2-alpine3.8

<!--more-->
## Examples

```yaml
php-security-checker:
  stage: linting
  image: pipelinecomponents/php-security-checker:latest
  script:
    - cd ${COMPOSER_LOCATION:-.} && security-checker security:check composer.lock
```
