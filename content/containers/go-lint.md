---
title: go-lint
anchor: go-lint
link: 'https://gitlab.com/pipeline-components/go-lint'
dockerimage: 'alpine:3.10.3'
---
The image is for running go-lint. The image is based on alpine:3.8

<!--more-->
## Examples

```yaml
go-lint:
  stage: linting
  image: pipelinecomponents/go-lint:latest
  script:
    - go-lint -v ./...
```
