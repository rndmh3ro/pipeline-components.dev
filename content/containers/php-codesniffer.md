---
title: php-codesniffer
anchor: php-codesniffer
link: 'https://gitlab.com/pipeline-components/php-codesniffer'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image is for running codesniffer, codesniffer is installed in /app/ in case you need to customize the install before usage.
Default the following packages are installed:

-   squizlabs/php_codesniffer
-   phpcompatibility/php-compatibility
-   dealerdirect/phpcodesniffer-composer-installer

The image is based on php:7.2-alpine3.8

<!--more-->
## Examples

```yaml
phpcs PSR2:
  stage: linting
  image: pipelinecomponents/php-codesniffer:latest
  script:
    - phpcs -s -p --colors --extensions=php --standard=PSR2 .
```

```yaml
php-compatibility 7.3:
  stage: test
  image: pipelinecomponents/php-codesniffer:latest
  variables:
    PHPVERSION: "7.3"
  script:
    - >-
      phpcs -s -p --colors
      --standard=PHPCompatibility
      --extensions=php
      --runtime-set testVersion ${PHPVERSION} .
```
