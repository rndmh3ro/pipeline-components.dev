---
title: php-linter
anchor: php-linter
link: 'https://gitlab.com/pipeline-components/php-linter'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image is for running phplinter, phplinter is installed in /app/ in case you need to customize the install before usage.
The image is based on php:7.3-alpine3.8

<!--more-->
## Example

```yaml
php linter:
  stage: linting
  image: pipelinecomponents/php-linter:latest
  script:
    - parallel-lint --colors .
```
