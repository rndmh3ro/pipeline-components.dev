---
title: awesome-lint
anchor: awesome-lint
link: 'https://gitlab.com/pipeline-components/awesome-lint'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running awesome-lint, awesome-lint is installed in /app/ in case you need to customize the install before usage. The image is based on alpine:3.8

<!--more-->
## Examples

```yaml
awesome-lint:
  stage: linting
  image: pipelinecomponents/awesome-lint:latest
  script:
    - awesome-lint .
```
