---
title: "Github"
anchor: "github"
---

No fully worked out example exists yet, we are still in the progress of giving all the components an github action

A example for yamllint for github action syntax and docker syntax is below

```yaml
name: CI

on:
  push:

jobs:
  yamllint:
    runs-on: ubuntu-latest
    steps:
      - name: Getting your configuration from GitHub
        uses: actions/checkout@v1
      - name: Running YAMLlint
        uses: "docker://pipelinecomponents/yamllint:latest"
        with:
          args: yamllint .
  yamllint-action:
    runs-on: ubuntu-latest
    steps:
      - name: Getting your configuration from GitHub
        uses: actions/checkout@v1
      - name: Running YAMLlint
        uses: pipeline-components/yamllint@master
```
