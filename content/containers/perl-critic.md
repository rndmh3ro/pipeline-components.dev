---
title: perl-critic
anchor: perl-critic
link: 'https://gitlab.com/pipeline-components/perl-critic'
dockerimage: 'alpine:3.10.3'
---
The image is for running perl-critic, perl-critic is installed in /app/ in case you need to customize the install before usage.
The image is based on alpine:3.8.

<!--more-->
## Examples

```yaml
perl-critic:
  stage: linting
  image: pipelinecomponents/perl-critic:latest
  script:
    - perlcritic .
```
