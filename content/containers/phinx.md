---
title: phinx
anchor: phinx
link: 'https://gitlab.com/pipeline-components/phinx'
dockerimage: 'php:7.3.11-alpine3.10'
---
The image is for running phinx, phinx is installed in /app/ in case you need to customize the install before usage.
The image is based on php:7.3-alpine3.8

<!--more-->
## Examples

TODO: example
